import ChatView from '../../Views/Chat';
import LoginView from '../../Views/Login';
import AboutMainView from '../../Views/About';
import LogoutView from '../../Views/Logout';
import UserView from '../../Views/User';

let Routes = [
    {
        path: '/',
        name: 'Login',
        component: LoginView
    },
    {
        path: '/Chat',
        name: 'Chat',
        component: ChatView
    },
    {
        path: '/About',
        name: 'About',
        component: AboutMainView
    },
    {
        path: '/Logout',
        name: 'Logout',
        component: LogoutView
    },
    {
        path: '/User',
        name: 'User',
        component: UserView
    }
];

export default Routes;