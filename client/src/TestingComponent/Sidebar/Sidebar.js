import React, { Component } from 'react'
import { withStyles, Hidden, Drawer } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import * as actions from '../../ReduxStore/Actions/DrawerActions';
import sidebarStyles from './sidebarStyles';
import DrawerItems from './DrawerItems';

class Sidebar extends Component {
  render() {
      const { classes, drawer, toggleDrawer } = this.props;
    return (
      <div>
        {/*mobile drawer*/}
        <Hidden mdUp>
            <Drawer
                variant="temporary"
                open={drawer.isOpen}
                onClose={() => toggleDrawer()}
                classes = {{
                    paper: classes.drawerPaper
                }}
            >
                <DrawerItems />
            </Drawer>
        </Hidden>

        {/*desktop drawer*/}
        <Hidden smDown>
            <Drawer
                variant="permanent"
                open
                classes = {{
                    paper: classes.drawerPaper
                }}
            >
                <DrawerItems />
            </Drawer>
        </Hidden>
      </div>
    )
  }
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
}

let mapStateToProps = ({drawer}) => {
    return {drawer};
}

export default connect(mapStateToProps, actions)(withStyles(sidebarStyles)(Sidebar));