let appStyles = theme => ({
    toolbar: {
        ...theme.mixins.toolbar,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    userAvatar: {
        height: 30,
        width: 30,
        marginBottom: 3
    },
    listItem: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        "&:hover": {
            backgroundColor: 'transparent',
            cursor: 'default',
        }
    }
});

export default appStyles;