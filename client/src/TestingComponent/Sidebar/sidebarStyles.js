import { drawerWidth } from '../../Assets/jss/common';

let sidebarStyles = theme => ({
    drawerPaper: {
        width: drawerWidth,
        height: '100vh',
        [theme.breakpoints.up('md')]: {
            position: 'fixed',
        },
        top: 0,
        left: 0,

    }
});

export default sidebarStyles;