import React, { Component } from "react";
import { PropTypes } from "prop-types";
import {
  withStyles,
  Divider,
  List,
  ListItem,
  Typography,
  Avatar
} from "@material-ui/core";
import { AccountCircle as AccountCircleIcon } from "@material-ui/icons";
import { connect } from "react-redux";

import drawerItemStyles from "./drawerItemsStyles";

class DrawerItems extends Component {
  render() {
    const {
      classes,
      ws: { userList }
    } = this.props;
    return (
      <div>
        <div className={classes.toolbar}>
          <Avatar className={classes.userAvatar}>
            <AccountCircleIcon />
          </Avatar>
        </div>
        <Divider />
        <List component="nav">
          {userList.length
            ? userList.map((prop, key) => {
                return (
                  <ListItem key={key} className={classes.listItem}>
                    <Typography
                      variant="body2"
                      style={{ color: prop.color}}
                    >
                      {prop.name}
                    </Typography>
                  </ListItem>
                );
              })
            : ""}
        </List>
      </div>
    );
  }
}

DrawerItems.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ ws }) => {
  return { ws };
};

export default connect(mapStateToProps)(
  withStyles(drawerItemStyles)(DrawerItems)
);
