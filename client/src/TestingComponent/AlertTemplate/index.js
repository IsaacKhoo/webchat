import React from "react";
import { Typography } from '@material-ui/core';

const alertTempalate = ({id, classNames, styles, message, customFields, handleClose,}) => {
   return (
      <div className={classNames} id={id} style={styles}>
         <Typography variant="body1" className="username">
            {customFields.username}
         </Typography>
         <Typography variant="body1" className="s-alert-box-inner" noWrap>
            {message}
         </Typography>
         <span className="s-alert-close" onClick={handleClose}></span>
      </div>
   );
}

export default alertTempalate;