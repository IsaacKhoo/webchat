import React, { Component, Fragment } from "react";
import { withStyles } from "@material-ui/core";
import { PropTypes } from "prop-types";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { connect } from "react-redux";
import Alert from 'react-s-alert';
import ifVisible from 'ifvisible.js';

import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import * as actions from "../../ReduxStore/Actions/WebSocketActions";
import appStyles from "./appStyles";
import Header from "../Header/Header";
import Sidebar from "../Sidebar/Sidebar";
import appRoutes from "../Routes/appRoutes";
import AlertTemplate from '../AlertTemplate';

class App extends Component {

  state ={
    pageVisible: true
  }

  componentDidMount() {
    if (this.props.ws.isConnected === false) {
      this.props.connectToServer();
    }
    window.addEventListener("beforeunload", event => {
      event.preventDefault();
      if (this.props.ws.isConnected === true) {
        this.props.logoutOfServer(this.props.ws.currentUsername);
      }
      return;
    });

    ifVisible.on('blur', () => {
      this.setState((prevState) => {
        return {...prevState, pageVisible: false}
      })
    })

    ifVisible.on('idle', () => {
      this.setState((prevState) => {
        return {...prevState, pageVisible: false}
      })
    })

    ifVisible.on('focus', () => {
      this.setState((prevState) => {
        return {...prevState, pageVisible: true}
      })
    })

    ifVisible.on('wakeup', () => {
      this.setState((prevState) => {
        return {...prevState, pageVisible: true}
      })
    })
  }

  componentWillUnmount() {
    window.removeEventListener("beforeunload");
  }

  render() {

    const { classes } = this.props;
    const { pageVisible } = this.state;

    return (
      <Router>
        <Fragment>
          <div className={classes.root}>
            <Sidebar />
            <div className={classes.mainContent}>
              <Header />
              <div className={classes.content}>
                {appRoutes.map((prop, key) => {
                  return (
                    <Route
                      exact
                      path={prop.path}
                      component={prop.component}
                      key={key}
                    />
                  );
                })}
              </div>
            </div>
          </div>
          {pageVisible 
            ? <Alert stack={false} beep='https://s3-ap-southeast-1.amazonaws.com/ocs.soundbucket/intuition.mp3' offset={-500} />
            : <Alert stack={{ limit: 3 }} beep='https://s3-ap-southeast-1.amazonaws.com/ocs.soundbucket/to-the-point.mp3' contentTemplate={AlertTemplate} /> }
        </Fragment>
      </Router>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ ws }) => {
  return { ws };
};

export default connect(
  mapStateToProps,
  actions
)(withStyles(appStyles)(App));
