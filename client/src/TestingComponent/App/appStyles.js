import { drawerWidth } from '../../Assets/jss/common';

let appStyles = theme => ({
    root: {
        width: '100vw',
        height: '100vh',
        display: 'flex',
        flexGrow: 1,
        position: 'relative',
        backgroundColor: 'red',
        overflow: 'hidden',
    },
    mainContent: {
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: `${drawerWidth}px`,
        },
        width: '100%',
        display: 'flex',
        position: 'relative',
        backgroundColor: '#e1eced',
    },
    content : {
        marginTop: '50px',
        minHeight: 'calc(100% - 100px)'
    }
});

export default appStyles;