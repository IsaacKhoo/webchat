let headerStyles = theme => ({
   appbar: {
      position: 'absolute',
   },
   toolbar: {
      ...theme.mixins.toolbar,
   },
   rightHeaderMenu: {
      flexGrow: 1,
   },
   headerTitleTypography: {
      textDecoration: 'none',
   },
   desktopHeaderMenuItems: {
      textDecoration: 'none',
      color: 'white'
   }
});

export default headerStyles;