let fadeMenuStyles = theme => ({
   menu: {
      position: 'fixed',
      [theme.breakpoints.up('sm')]: {
         top: 50,
      },
      top: 40,
   },
   fadeMenuTypography: {
      textDecoration: 'none',
   }
});

export default fadeMenuStyles;