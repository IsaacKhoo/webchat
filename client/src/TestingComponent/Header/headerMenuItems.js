export let menuItems = [
   {
      name: 'User',
      path: '/User',
   },
   {
      name: 'About',
      path: '/About',
   },
   {
      name: 'Logout',
      path: '/Logout',
   }
]