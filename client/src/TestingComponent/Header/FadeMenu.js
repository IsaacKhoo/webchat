import React from "react";
import {
  Menu,
  MenuItem,
  Fade,
  IconButton,
  Typography,
  withStyles
} from "@material-ui/core";
import { MoreVert as MoreVertIcon } from "@material-ui/icons";
import { PropTypes } from "prop-types";
import { Link } from 'react-router-dom';

import fadeMenuStyles from "./fadeMenuStyles";

class FadeMenu extends React.Component {
  state = {
    anchorEl: null
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { menuItems, classes } = this.props;
    const { anchorEl } = this.state;
    return (
      <div>
        <IconButton
          aria-owns={anchorEl ? "fade-menu" : null}
          aria-haspopup="true"
          onClick={this.handleClick}
          color="inherit"
        >
          <MoreVertIcon />
        </IconButton>

        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          TransitionComponent={Fade}
          className={classes.menu}
        >
          {menuItems.map((props, key) => {
            return (
              <MenuItem onClick={this.handleClose} key={key}>
                  <Typography 
                    variant="subheading" 
                    component={Link} 
                    to={props.path}
                    className={classes.fadeMenuTypography}
                  >
                    {props.name}
                  </Typography>
              </MenuItem>
            );
          })}
        </Menu>
      </div>
    );
  }
}

FadeMenu.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(fadeMenuStyles)(FadeMenu);
