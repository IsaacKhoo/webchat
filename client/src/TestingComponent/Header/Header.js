import React, { Component } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Hidden,
  IconButton,
  withStyles,
  Grid,
  Button
} from "@material-ui/core";
import { Menu as MenuIcon } from "@material-ui/icons";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import * as actions from "../../ReduxStore/Actions/DrawerActions";
import headerStyles from "./headerStyles";
import { menuItems as headerMenuItems } from "./headerMenuItems";

import FadeMenu from "./FadeMenu";

class Header extends Component {
  render() {
    const { classes, toggleDrawer, appTitle, ws } = this.props;

    return (
      <AppBar className={classes.appbar}>
        <Toolbar className={classes.toolbar}>
          <Grid
            container
            spacing={0}
            className={classes.rightHeaderMenu}
            direction="row"
          >
            <Hidden mdUp>
              <Grid item container xs={2} justify="flex-start">
                <IconButton color="inherit" onClick={() => toggleDrawer()}>
                  <MenuIcon />
                </IconButton>
              </Grid>
            </Hidden>

            <Grid item container xs justify="flex-start" alignItems="center">
              <Typography
                variant="title"
                color="inherit"
                component={Link}
                to={"/"}
                className={classes.headerTitleTypography}
              >
                {appTitle || "Web Chat"}
              </Typography>
            </Grid>

            {/*Mobile menu*/}
            <Hidden mdUp>
              {ws.currentUsername !== "" ? (
                <Grid item container xs={2} justify="flex-end">
                  <FadeMenu menuItems={headerMenuItems} />
                </Grid>
              ) : null}
            </Hidden>

            {/*Desktop menu*/}
            <Hidden smDown>
              <Grid
                item
                container
                xs={3}
                justify="flex-end"
                direction="row"
                spacing={0}
              >
                {ws.currentUsername !== ""
                  ? headerMenuItems.map((prop, key) => {
                      return (
                        <Grid item xs={3} key={key}>
                          <Button
                            variant ="text"
                          >
                            <Typography
                              variant="subheading"
                              component={Link}
                              to={prop.path}
                              className={classes.desktopHeaderMenuItems}
                            >
                              {prop.name}
                            </Typography>
                          </Button>
                        </Grid>
                      );
                    })
                  : null}
              </Grid>
            </Hidden>
          </Grid>
        </Toolbar>
      </AppBar>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ drawer, ws }) => {
  return { drawer, ws };
};

export default connect(
  mapStateToProps,
  actions
)(withStyles(headerStyles)(Header));
