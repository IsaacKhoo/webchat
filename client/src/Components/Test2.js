import React, { Component } from 'react'
import { Paper, List, Button, ListItem, ListItemText } from '@material-ui/core';

let array = [
    {
        msg: 'item1',
    },
    {
        msg: 'item2',
    },
    {
        msg: 'item3',
    },
    {
        msg: 'item4',
    },
    {
        msg: 'item5',
    },
    
];

class Test2 extends Component {
    state = {
        value: 6,
    }

    scrollToBottom = () => {
        this.end.scrollIntoView({ behavior: 'smooth' });
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    render() {
        return (
            <div style={{
                border: '1px black solid'
            }}>
                <Button
                    onClick={() => {
                        array.push({
                            msg: `item${this.state.value}`
                        });
                        this.setState({value: this.state.value + 1});
                    }}
                    style={{
                        position: 'fixed',
                        top: 75,
                        left: -10,
                        zIndex: 2000,
                    }}
                    variant='contained'
                    
                >
                    add div
                </Button>

                <List>
                    {array.map((prop, key) => {
                        return (
                            <div style={{ height: '500px' }} ref={section => (this.msg = section)} key={key}>
                                <ListItem>
                                    <Paper>
                                        <ListItemText primary={prop.msg} />
                                    </Paper>
                                </ListItem>                    
                            </div>
                        );                
                    })}
                    <div ref={section => (this.end = section)}></div>
                </List>

            </div>
        )
    }
}

export default Test2;
