import React, { Component, Fragment } from "react";
import { withStyles, Hidden, Drawer, Divider } from "@material-ui/core";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";

import * as drawerActions from "../Actions/DrawerActions";
import sidebarStyles from "../Assets/jss/sidebarStyles";

class Sidebar extends Component {
  render() {
    const { classes, users, drawer, toggleDrawer } = this.props;
    return (
      <div>
        <div>
          {/*drawer for mobile*/}
          <Hidden mdUp>
            <Drawer
              variant="temporary"
              anchor="left"
              open={drawer.isOpen}
              onClose={() => toggleDrawer()}
              classes={{
                paper: classes.drawerPaper
              }}
              ModalProps={{
                keepMounted: true
              }}
            >
              <div className={classes.toolbar} />
              <Divider />
              {users.userList}
            </Drawer>
          </Hidden>

          {/*drawer for desktop*/}
          <Hidden smDown>
            <Drawer
              variant="permanent"
              open
              classes={{
                paper: classes.drawerPaper
              }}
            >
              <div className={classes.toolbar} />
              <Divider />
              {users.userList}
            </Drawer>
          </Hidden>
        </div>
      </div>

    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ drawer, users }) => {
  return { drawer, users };
};

export default connect(
  mapStateToProps,
  drawerActions
)(withStyles(sidebarStyles)(Sidebar));
