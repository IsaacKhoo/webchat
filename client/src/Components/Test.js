import React, { Component } from 'react'
import { List, ListItem, Paper, ListItemText } from '@material-ui/core';


class Test extends Component {

    componentDidUpdate() {
        // setTimeout(() => {
        //   document.querySelector('#chatArea').scrollTo(0,document.querySelector('#chatArea').scrollHeight)
        // }, 100)

        document.querySelector('#chatEnd').scrollIntoView();
      }

    render() {
        const { msgArray } = this.props;
        return (
            <div>
                <List id='chatArea'>
                    {msgArray.map((prop, key) => {
                        return (
                            <div ref={el => (this.item = el)} key={key}>
                                <ListItem>
                                    <Paper>
                                        <ListItemText primary={prop.message} secondary={prop.timeStamp} />
                                    </Paper>
                                </ListItem>
                            </div>
                        );
                    })}
                    <div id='chatEnd' ref={el => (this.end = el)}></div>
                </List>
            </div>
        )
    }
}

export default Test;