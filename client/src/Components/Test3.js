import React, { Component } from 'react'
import { Button } from '@material-ui/core';
import Scroll from 'react-scroll';


var Link = Scroll.Link;
var DirectLink = Scroll.DirectLink;
var Element = Scroll.Element;
var Events = Scroll.Events;
var scroll = Scroll.animateScroll;
var scrollSpy = Scroll.scrollSpy;

class Test3 extends Component {

    state = {
        value: 0,
    }

    componentDidMount() {

        Events.scrollEvent.register('begin', function () {
            console.log("begin", arguments);
        });

        Events.scrollEvent.register('end', function () {
            console.log("end", arguments);
        });

        scrollSpy.update();

    }

    componentWillUnmount() {
        Events.scrollEvent.remove('begin');
        Events.scrollEvent.remove('end');
    }

    componentWillUpdate() {
        document.querySelector('#scroller').click();
    }

    render() {
        return (
            <div>
               
               <Button
                onClick={() => {
                    this.setState({value: this.state.value++})
                }}
                style={{
                    zIndex: 2000
                }}
                variant="contained"
               >
                click here
               </Button>

    <Link id="scroller" activeClass="active" to="secondInsideContainer" spy={true} smooth={true} duration={250} containerId="containerElement" style={{ display: 'inline-block', margin: '20px' }}>
        </Link>
                <Element name="test7" className="element" id="containerElement" style={{
                    position: 'relative',
                    height: '200px',
                    overflow: 'scroll',
                    marginBottom: '100px'
                }}>
                    test 7 (duration and container)
          
          <Element name="firstInsideContainer" style={{
                        marginBottom: '200px'
                    }}>
                        first element inside container
          </Element>

                    <Element id="secondInsideContainer" name="secondInsideContainer" style={{
                        marginBottom: '200px'
                    }}>
                        second element inside container
          </Element>
                </Element>
            </div>
        )
    }
}

export default Test3;