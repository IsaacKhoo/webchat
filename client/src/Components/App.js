import React, { Component } from "react";
import { withStyles, Button } from "@material-ui/core";
import { PropTypes } from "prop-types";

import appStyles from "../Assets/jss/appStyles";
import Header from "./Header";
import Sidebar from "./Sidebar";
import ChatArea from "./ChatArea";
import Test from './Test';
import Test2 from './Test2';
import Test3 from './Test3';

class App extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Header />
        <Sidebar />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Test3 />
        </main>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(appStyles)(App);
