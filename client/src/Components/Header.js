import React, { Component } from "react";
import {
  AppBar,
  Toolbar,
  withStyles,
  IconButton,
  Hidden,
  Typography
} from "@material-ui/core";
import { Menu as MenuIcon } from "@material-ui/icons";
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

import * as drawerActions from '../Actions/DrawerActions';
import headerStyles from "../Assets/jss/headerStyles";

class Header extends Component {
  render() {
    const { classes, title, toggleDrawer } = this.props;
    return (
      <div>
        <AppBar className={classes.appBar}>
          <Toolbar>

            <Hidden mdUp>
              <IconButton
                color="inherit"
                onClick={() => toggleDrawer()}
              >
                <MenuIcon />
              </IconButton>
            </Hidden>

            <Typography
              variant="title"
              color="inherit"
              noWrap
            >
              {title || "WebChat"}
            </Typography>
          </Toolbar>
        </AppBar>
      </div>

    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
}

let mapStateToprops = ({ drawer }) => {
  return { drawer };
}

export default connect(mapStateToprops, drawerActions)(withStyles(headerStyles)(Header));
