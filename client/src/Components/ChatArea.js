import React, { Component } from "react";
import { withStyles, TextField, IconButton, Paper, Grid, Typography, List, ListItem, ListItemText } from "@material-ui/core";
import sendArrowIcon from "../Assets/img/sharp_send_black_36dp.png";
import { PropTypes } from "prop-types";

import chatAreaStyles from "../Assets/jss/chatareaStyles";
import Test from './Test';

let getMessage = msg => {
  let date = new Date();
  let newMessageObject = {
    message: msg,
    user: 'user1',
    timestamp: `${date.getHours()}:${date.getMinutes()}`,
  };
  return newMessageObject;
};

let messageList = [
  {
    message: "hi!",
    user: 'user2',
    timestamp: "11:00",
  },
  {
    message: "Wanna talk?",
    user: 'user2',
    timestamp: "11:01",
  }
];

let addToMessageList = (msgObj) => {
  messageList.push(msgObj);
}

class ChatArea extends Component {
  state = {
    textMessage: "",
    latestMsg: '',
  };

  postMessage() {
    let newMessage = getMessage(this.state.textMessage);
    addToMessageList(newMessage);
    this.setState(prevState => {
      return { ...prevState, textMessage: "", latestMsg: messageList };
    });
  }

  handleTextChange = (event) => {
    let currentInput = event.target.value;
    if (currentInput.length === 1 && currentInput === "\n") {
      currentInput = "";
    }
    if (currentInput[currentInput.length - 1] === "\n")
      currentInput += '\r\n';
    let newState = { ...this.state, textMessage: currentInput };
    this.setState(newState);
  }

  focusInputField = () => {
    this.TextField.focus();
  }

  componentDidUpdate() {
      document.querySelector('#chatEnd').scrollIntoView();

  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <div className={classes.chatBox} >
          <List className={classes.listContainer} id="chatArea">
            {messageList.map((prop, key) => {
              return (
                <div ref={section => (this.msg = section)} key={key}>
                  <ListItem className={prop.user === 'user1' ? classes.userMsg : classes.otherMsg}>
                    <Paper className={classes.chatPaper}>
                      <ListItemText primary={prop.message} secondary={prop.timestamp} />
                    </Paper>
                  </ListItem>
                </div>
              )
            })}
            <div id='chatEnd' ref={section => (this.end = section)}></div>
          </List>
        </div>
        {/* <div className={classes.chatBox}>
          <Test msgArray={messageList}  />
        </div>  */}
        <div>
          <Paper square className={classes.inputPaper}>
            <TextField
              id="inputField"
              multiline
              rowsMax="2"
              placeholder="input text here"
              className={classes.inputField}
              value={this.state.textMessage}
              onChange={(e) => this.handleTextChange(e)}
              onKeyPress={e => {
                if (e.key === 'Enter' && !e.shiftKey) {
                  this.postMessage();
                }
              }}
              inputRef={field => this.TextField = field}
            /* key press and key down events do not work on mobile.
            */
            // onKeyPress={e => {
            //   if (e.key === "Enter" && !e.shiftKey) {
            //     getMessage(this.state.textMessage);
            //     this.setState(prevState => {
            //       return { ...prevState, textMessage: "" };
            //     });
            //   } else if (e.key === "Enter" && e.shiftKey) {
            //     let newState = {...this.state, textMessage: this.state.textMessage + "\r\n"};
            //     this.setState(newState);
            //   } else {
            //     let newState = {...this.state, textMessage: this.state.textMessage + e.key};
            //     this.setState(newState);
            //   }
            // }}
            // onKeyDown={e => {
            //   if(e.keyCode === 8) {
            //     let newState = {...this.state, textMessage: this.state.textMessage.substring(0,this.state.textMessage.length - 1)};
            //     this.setState(newState);
            //   }
            // }}
            />

            <IconButton
              className={classes.inputSendButton}
              onClick={(e) => {
                e.preventDefault();
                this.postMessage();
                this.focusInputField();
              }}
            >
              <img
                src={sendArrowIcon}
                alt=""
                style={{ height: "25px", width: "25px" }}
              />
            </IconButton>
          </Paper>
        </div>

      </div>
    );
  }
}

ChatArea.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(chatAreaStyles)(ChatArea);
