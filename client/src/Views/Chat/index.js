import React, { Component } from "react";
import {
  List,
  ListItem,
  Paper,
  withStyles,
  TextField,
  IconButton,
  Typography
} from "@material-ui/core";
import { Send as SendIcon } from "@material-ui/icons";
import Scroll from "react-scroll";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import * as wsActions from "../../ReduxStore/Actions/WebSocketActions";
import chatViewStyles from "./chatViewStyles";


var Link = Scroll.Link;
var Element = Scroll.Element;
var scrollSpy = Scroll.scrollSpy;

let isMobileDevice = () => {
  return (
    typeof window.orientation !== "undefined" ||
    navigator.userAgent.indexOf("IEMobile") !== -1
  );
};

let createMessageObject = (message, username) => {
  let date = new Date();
  let hour = date.getHours();
  let minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();
  let newMessageObject = {
    msg: message,
    user: username,
    timestamp: `${hour}:${minutes}`
  };
  return newMessageObject;
};

class ChatView extends Component {
  state = {
    isMobile: false,
    textMessage: ""
  };

  componentDidMount() {
    scrollSpy.update();

    this.setState(prevState => {
      return { ...prevState, isMobile: isMobileDevice() };
    });
  }

  componentWillUpdate() {
    document.querySelector("#scroller").click();
  }

  handleTextChange = event => {
    let currentInput = event.target.value;
    if (currentInput.length === 1 && currentInput === "\n") {
      currentInput = "";
    }
    let newState = { ...this.state, textMessage: currentInput };
    this.setState(newState);
  };

  postMessage(username) {
    if (this.state.textMessage.length > 0) {
      let newMessage = createMessageObject(this.state.textMessage, username);
      this.setState(prevState => {
        return { ...prevState, textMessage: "" };
      });
      this.props.sendMessage(newMessage);
    }
  }

  focusInputField = () => {
    this.TextField.focus();
  };

  redirectIfNotLoggedIn(userState) {
    if (userState === "") {
      return <Redirect to="/" />;
    }
  }

  getUserColor(name) {
    let userList = this.props.ws.userList;
    for (let i = 0; i < userList.length; i++) {
      if (userList[i].name === name) {
        return userList[i].color;
      }
    }
    return "#FFFFFF";
  }

  render() {
    const {
      classes,
      ws: { currentUsername, messageList }
    } = this.props;

    return (
      <div>
        {this.redirectIfNotLoggedIn(currentUsername)}

        <Link
          id="scroller"
          activeClass="active"
          to="chatEnd"
          spy={true}
          smooth={true}
          duration={1000}
          containerId="containerElement"
        />

        <Element
          name="test7"
          id="containerElement"
          className={classes.chatArea}
        >
          <List disablePadding dense>
            {messageList.length
              ? messageList.map((prop, key) => {
                  return (
                    <ListItem
                      key={key}
                      className={
                        prop.user === currentUsername
                          ? classes.userMessage
                          : classes.otherMessage
                      }
                    >
                      <Paper
                        className={
                          prop.user === currentUsername
                            ? classes.userPaperStyles
                            : classes.otherPaperStyles
                        }
                      >
                        <Typography
                          variant="caption"
                          style={{ color: this.getUserColor(prop.user) }}
                        >
                          {prop.user === currentUsername ? null : prop.user}
                        </Typography>
                        <div style={{ display: "flex" }}>
                          <Typography
                            variant="body1"
                            className={classes.typographyMessage}
                          >
                            {prop.msg}
                          </Typography>
                          <Typography
                            variant="caption"
                            className={classes.typographyTimestamp}
                          >
                            {prop.timestamp}
                          </Typography>
                        </div>
                      </Paper>
                    </ListItem>
                  );
                })
              : ""}
            <Element name="chatEnd" />
          </List>
        </Element>

        <Paper className={classes.inputPaper}>
          <TextField
            multiline
            rowsMax="2"
            placeholder="input text here"
            value={this.state.textMessage}
            onChange={e => this.handleTextChange(e)}
            onKeyPress={e => {
              if (e.key === "Enter" && !e.shiftKey) {
                if (this.state.isMobile === false) {
                  document.querySelector("#sendIconButton").click();
                }
              }
            }}
            className={classes.inputTextArea}
            inputRef={field => (this.TextField = field)}
          />
          <IconButton
            id="sendIconButton"
            className={classes.inputIconButton}
            onClick={e => {
              e.preventDefault();
              this.postMessage(currentUsername);
              this.focusInputField();
            }}
            disableRipple
          >
            <SendIcon />
          </IconButton>
        </Paper>
      </div>
    );
  }
}

ChatView.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ ws }) => {
  return { ws };
};

export default connect(
  mapStateToProps,
  wsActions
)(withStyles(chatViewStyles)(ChatView));
