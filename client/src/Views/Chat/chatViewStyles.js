let chatViewStyles = theme => ({
    chatArea:{
        [theme.breakpoints.up('md')]: {
            padding: '20px 75px 20px 0px',
            height: 'calc(100% - 120px)',
        },
        padding: '10px 2px',     
        overflow: 'hidden',
        width: '100%',
        height: `calc(100% - 170px)`, 
        overflowY: 'scroll',
        position: 'absolute',
    },
    userMessage: {
        padding: '3px 20px 3px 10px',
        justifyContent: 'flex-end',       
    },
    otherMessage: {
        padding: '3px 10px 3px 20px',
        justifyContent: 'flex-start',
    },
    otherPaperStyles: {
        [theme.breakpoints.up('md')]:{
            maxWidth:'45%',
        },
        padding: '5px 10px',
        maxWidth: '60%',
    },
    userPaperStyles:{
        backgroundColor: '#c4f2c1',
        [theme.breakpoints.up('md')]:{
            maxWidth:'45%',
        },
        padding: '5px 10px',
        maxWidth: '60%',

    },
    typographyMessage: {
        wordWrap: 'break-word',
        whiteSpace: 'pre-line',
    },
    typographyTimestamp:{
        alignSelf: 'flex-end',
        marginLeft: '5px',
    },
    inputPaper: {
        [theme.breakpoints.up('md')]: {
            position: 'absolute',
        },
        position: 'fixed',
        bottom: 0,
        left: 0,
        right: 0,
        padding: '2px 10px 0px',
        display: 'flex',
    },
    inputTextArea: {
        flex: 9,
    },
    inputIconButton: {
        flex: 1,
        "&:hover": {
            backgroundColor: 'transparent'
        }
    }
});

export default chatViewStyles;