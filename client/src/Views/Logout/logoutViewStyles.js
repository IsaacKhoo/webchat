let logoutViewStyles = theme => ({
  logoutRoot: {
    [theme.breakpoints.up("md")]: {
      height: "100%"
    },
    position: "absolute",
    backgroundColor: "sky-blue",
    width: "100%",
    height: "85%",
    display: "flex",
    flexGrow: 1,
    alignContent: "center",
    alignItems: "center",
    justifyContent: "center",
  },
  headerTypography:{
   margin: '20px',
  },
  centraliseDiv: {
   display: "flex",
   flexGrow: 1,
   alignContent: "center",
   alignItems: "center",
   justifyContent: "center",
  },
  button: {
     flex: 1,
     margin: '10px'
  }
});

export default logoutViewStyles;
