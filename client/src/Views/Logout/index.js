import React, { Component } from "react";
import { withStyles, Typography, Paper, Button, Grid } from "@material-ui/core";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import * as WSActions from "../../ReduxStore/Actions/WebSocketActions";
import logoutViewStyles from "./logoutViewStyles";

class LogoutView extends Component {

   state = {
      redirect: false
   }

  redirectIfNotLoggedIn(userState) {
    if (userState === "") {
      return <Redirect to="/" />;
    }
  }

  handleCancelClick() {
     this.setState((prevState) => {
        return {...prevState, redirect: true};
     })
  }

  handleLogoutClick() {
   this.props.logoutOfServer();
  }

  render() {
    const {
      classes,
      ws: { currentUsername }
    } = this.props;
    const {redirect} = this.state;

    return (
      <div className={classes.logoutRoot}>
        {this.redirectIfNotLoggedIn(currentUsername)}
        {redirect? <Redirect to="/Chat" /> : null}
        <Paper>
          <Typography variant="subheading" color="inherit" align="center" className={classes.headerTypography}>
            Logout from {currentUsername}?
          </Typography>

          <div className={classes.centraliseDiv}>
            <div className={classes.button}>
              <Button variant="outlined" onClick={() => this.handleCancelClick()}>Cancel</Button>
            </div>
            <div className={classes.button}>
              <Button variant="outlined" onClick={() => this.handleLogoutClick()}>Logout</Button>
            </div>
          </div>
        </Paper>
      </div>
    );
  }
}

LogoutView.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ ws }) => {
  return { ws };
};

export default connect(
  mapStateToProps,
  WSActions
)(withStyles(logoutViewStyles)(LogoutView));
