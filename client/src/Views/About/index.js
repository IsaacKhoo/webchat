import React, { Component } from "react";
import {
  withStyles,
  Typography,
  Grid,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  CircularProgress,
  Card,
  CardHeader,
  Avatar,
  IconButton,
  Collapse,
  Divider
} from "@material-ui/core";
import PropType from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import {
  ExpandMore as ExpandMoreIcon,
  CheckCircle as CheckCircleIcon,
  GetApp as GetAppIcon
} from "@material-ui/icons";

import aboutMainStyles from "./aboutMainStyles";
import * as aboutActions from "../../ReduxStore/Actions/AboutActions";

class AboutMain extends Component {
  retrieveFrontEndContent() {
    if (this.props.abt.frontEndContent.length === 0) {
      this.props.getFrontendTools();
    } else {
      console.log(`content retrieved: ${this.props.abt.frontEndContent}`);
      return;
    }
  }

  retrieveBackEndContent() {
    if (this.props.abt.backEndContent.length === 0) {
      this.props.getBackendTools();
    } else {
      console.log(`content retrieved: ${this.props.abt.backEndContent}`);
      return;
    }
  }

  redirectIfNotLoggedIn(userState) {
    if (userState === "") {
      return <Redirect to="/" />;
    }
  }

  render() {
    const {
      classes,
      abt: {
        frontEndContent,
        backEndContent,
        frontEndLoading,
        backEndLoading,
        errorMsg,
        frontEndShowError,
        backEndShowError,
        displayFrontendCollapse,
        displayBackendCollapse
      },
      ws: { currentUsername }
    } = this.props;

    return (
      <div className={classes.aboutMain}>
        {this.redirectIfNotLoggedIn(currentUsername)}
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Typography
              color="inherit"
              align="center"
              className={classes.introTypography}
            >
              This bit of code is rendered on demand by the server.
            </Typography>
            <Typography
              color="inherit"
              align="center"
              className={classes.subIntroTypography}
            >
              Tap download button to render tools used.
            </Typography>
          </Grid>

          <Grid item xs={12} md={6}>
            <Card>
              <CardHeader
                avatar={<Avatar className={classes.frontendAvatar}>F</Avatar>}
                action={
                  <IconButton onClick={() => this.retrieveFrontEndContent()}>
                    {displayFrontendCollapse? <CheckCircleIcon className={classes.checkCircleIcon} /> : <GetAppIcon />}
                    {frontEndLoading && (
                      <CircularProgress size={48} className={classes.loader} />
                    )}
                  </IconButton>
                }
                title="Front End Tools Used"
                subheader="Injected false retrieval delay of 2 seconds"
              />

              <Collapse in={displayFrontendCollapse}>
              <Divider />
                {frontEndContent.map((prop, key) => {
                  return (
                    <ExpansionPanel key={key}>
                      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography variant="body2">{prop.title}</Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails>
                        <Typography paragraph variant="caption">{prop.description}</Typography>            
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  );
                })}
              </Collapse>

              <Collapse in={frontEndShowError}>
                <Typography variant="body2" color="error">
                  {errorMsg}
                </Typography>
              </Collapse>
            </Card>
          </Grid>

          <Grid item xs={12} md={6}>
            <Card>
              <CardHeader
                avatar={<Avatar className={classes.backendAvatar}>B</Avatar>}
                action={
                  <IconButton onClick={() => this.retrieveBackEndContent()}>
                    {displayBackendCollapse? <CheckCircleIcon className={classes.checkCircleIcon} /> : <GetAppIcon /> }
                    {backEndLoading && (
                      <CircularProgress size={48} className={classes.loader} />
                    )}
                  </IconButton>
                }
                title="Back End Server Tools"
                subheader="No delay injected"
              />

              <Collapse in={displayBackendCollapse}>
              <Divider />
                {backEndContent.map((prop, key) => {
                  return (
                    <ExpansionPanel key={key}>
                      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography variant="body2">{prop.title}</Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails>
                        <Typography paragraph variant="caption">{prop.description}</Typography>            
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  );
                })}
              </Collapse>

              <Collapse in={backEndShowError}>
                <Typography variant="body2" color="error">
                  {errorMsg}
                </Typography>
              </Collapse>
            </Card>
          </Grid>
        </Grid>
      </div>
    );
  }
}

aboutMainStyles.propType = {
  classes: PropType.object.isRequired
};

let mapStateToProps = ({ abt, ws }) => {
  return { abt, ws };
};

export default connect(
  mapStateToProps,
  aboutActions
)(withStyles(aboutMainStyles)(AboutMain));
