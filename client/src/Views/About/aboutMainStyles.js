import { colors } from '@material-ui/core';

let aboutMainStyles = theme => ({
   aboutMain: {
      [theme.breakpoints.up('md')]:{
         height: '100%',
         overflow: 'hidden',
      },
      position: 'absolute',
      paddingLeft: '10%',
      paddingRight: '10%',
      height: '85%',
      display: 'flex',
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'center',
      overflow: 'scroll',
      overflowScrolling: 'touch',
   },
   introTypography: {
      [theme.breakpoints.up('md')]: {
         fontSize: '2vw',
      },
      fontSize: '5vw',
   },
   subIntroTypography: {
      [theme.breakpoints.up('md')]: {
         fontSize: '1vw',
      },
      fontSize: '3vw',
   },
   frontendAvatar: {
      backgroundColor: colors.blue[200],
   },
   backendAvatar:{
      backgroundColor: colors.green[300],
   },
   loader: {
      position: 'absolute',
      color: colors.cyan[500],
   },
   checkCircleIcon: {
      color: colors.green[500],
   }
});

export default aboutMainStyles;