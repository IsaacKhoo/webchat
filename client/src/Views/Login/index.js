import React, { Component } from 'react'
import { withStyles, Paper, Typography, TextField, Button } from '@material-ui/core';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router-dom';

import * as wsActions from '../../ReduxStore/Actions/WebSocketActions';
import loginViewStyles from './loginViewStyles';
const cookie = new Cookies();

class LoginView extends Component {
    state = {
        loginButtonDisabled: true,
        usernameField: '',
    }

    handleTextChange(event) {
        let usernameFieldText = event.target.value;
        if (usernameFieldText.length === 1 && usernameFieldText === "\n") {
            usernameFieldText = "";
        }
        if(usernameFieldText.length === 0) {
            let newState = {...this.state, loginButtonDisabled: true, usernameField: usernameFieldText};
            this.setState(newState);
        }
        else {
            let newState = {...this.state, loginButtonDisabled: false, usernameField: usernameFieldText};
            this.setState(newState);
        }
    }

    submitUsername() {
        let username = this.state.usernameField;
        this.props.loginToServer(username);
        this.setState((prevState) => {
            return {...prevState, usernameField: ''};
        })
        cookie.set('usernames', username, {maxAge: 1 });
    }

    redirectIfLoggedIn(userState) {
        if(userState !== '') {
          return <Redirect to="/chat" />;
        }
      }

    render() {

        //eslint-disable-next-line
        const { classes, ws: {currentUsername} } = this.props;
        const { loginButtonDisabled, usernameField } = this.state;

        return (
            <div className={classes.loginArea}>

                {this.redirectIfLoggedIn(currentUsername)}

                {cookie.get('usernames')
                    ?
                    <Paper className={classes.loginPaper} elevation={5}>
                        Loading..
                    </Paper>
                    :
                    <Paper className={classes.loginPaper} elevation={5}>
                        <Typography variant="subheading" color="inherit">
                            Login
                        </Typography>
                        <TextField
                            label="Enter new username"
                            placeholder="username"
                            className={classes.usernameInputField}
                            value={usernameField}
                            onChange={(e) => this.handleTextChange(e)}
                            onKeyPress={e => {
                                if (e.key === 'Enter' && !e.shiftKey) {
                                    document.querySelector('#submitButton').click();
                                }
                              }}
                        />
                        <Button
                            id= "submitButton"
                            disabled={loginButtonDisabled}
                            color="inherit"
                            variant="outlined"
                            onClick={e => {
                                e.preventDefault();
                                this.submitUsername();
                            }}
                        >
                            Login
                            </Button>
                    </Paper>
                }

            </div>
        )
    }
}

LoginView.propTypes = {
    classes: PropTypes.object.isRequired,
}

let mapStateToProps = ({ws}) => {
    return {ws};
}

export default connect(mapStateToProps, wsActions)(withStyles(loginViewStyles)(LoginView));
