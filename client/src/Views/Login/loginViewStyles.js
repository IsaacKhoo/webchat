let loginViewStyles = theme => ({
    loginArea: {
        [theme.breakpoints.up('md')]: {
            height: '100%',
        },
        position: 'absolute',
        backgroundColor: 'sky-blue',
        width: '100%',
        height: '85%',
        display: 'flex',
        flexGrow: 1,
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginPaper: {
        width: '250px',
        display: 'flex',
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        padding: '10px 5px',
    },
    usernameInputField: {
        width: '80%',
        margin: '15px 0px'
    }
});

export default loginViewStyles;