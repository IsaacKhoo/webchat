import React, { Component } from "react";
import { withStyles, Typography, Paper, Button, Grid } from "@material-ui/core";
import { PropTypes } from "prop-types";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import * as WSActions from "../../ReduxStore/Actions/WebSocketActions";
import userViewStyles from "./userViewStyles";

class userView extends Component {
  redirectIfNotLoggedIn(userState) {
    if (userState === "") {
      return <Redirect to="/" />;
    }
  }

  render() {
    const {
      classes,
      ws: { currentUsername, onlineTime, messagesSent }
    } = this.props;

    return (
      <div className={classes.userViewRoot}>
        {this.redirectIfNotLoggedIn(currentUsername)}
        <Paper className={classes.paper}>
          <Grid container spacing={16}>

            <Grid item xs={6}>
              <Typography variant="body2" color="inherit" align="left">
                Username
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant='body1' color="inherit" align="right">
                {currentUsername}
              </Typography>
            </Grid>

            <Grid item xs={6}>
              <Typography variant="body2" color="inherit" align="left">
                Login Time
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant='body1' color="inherit" align="right">
                {onlineTime}
              </Typography>
            </Grid>

            <Grid item xs={6}>
              <Typography variant="body2" color="inherit" align="left">
                Messages Sent
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant='body1' color="inherit" align="right">
                {messagesSent}
              </Typography>
            </Grid>

          </Grid>
        </Paper>
      </div>
    );
  }
}

userView.propTypes = {
  classes: PropTypes.object.isRequired
};

let mapStateToProps = ({ ws }) => {
  return { ws };
};

export default connect(
  mapStateToProps,
  WSActions
)(withStyles(userViewStyles)(userView));
