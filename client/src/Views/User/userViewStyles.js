let userViewStyles = theme => ({
   userViewRoot: {
      [theme.breakpoints.up("md")]: {
         height: "100%"
       },
       position: "absolute",
       backgroundColor: "sky-blue",
       width: "100%",
       height: "85%",
       display: "flex",
       flexGrow: 1,
       alignContent: "center",
       alignItems: "center",
       justifyContent: "center",
   },
   paper: {
      minWidth: '30%',
      padding: '10px'
   },
 });
 
 export default userViewStyles;
 