import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
//import { socketMiddleware } from '../Middleware/WebsocketMiddleware';
import createMiddleware from '../Middleware/myMiddleware';

import reducers from '../Reducers';

let store = createStore(reducers, {}, applyMiddleware(createMiddleware, thunk));

export default store;