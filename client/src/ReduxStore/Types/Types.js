// Drawer
const TOGGLE_DRAWER = 'toggle_drawer';

// user
const UPDATE_USER_LIST = "update_user_list";
const LOGIN = 'login';
const LOGOUT = 'logout';

// chat
const POST_CHAT = "post_chat";
const UPDATE_CHAT = "update_chat";

// about
const RETRIEVE_FRONTEND_TOOLS = "retrieve_frontend_tools";
const RETRIEVE_BACKEND_TOOLS = "retrieve_backend_tools";
const SET_FRONTEND_LOADER = "set_frontend_loader";
const SET_BACKEND_LOADER = "set_backend_loader";
const FRONTEND_ERROR_ENCOUNTERED = "frontend_error_encountered";
const BACKEND_ERROR_ENCOUNTERED = 'backend_error_encountered';
const TOGGLE_FRONTEND_COLLPASE = "toggle_frontend_collapse";
const TOGGLE_BACKEND_COLLPASE = "toggle_backend_collpase";

export {
   TOGGLE_DRAWER,
   UPDATE_USER_LIST,
   LOGIN,
   LOGOUT,
   POST_CHAT,
   UPDATE_CHAT,
   RETRIEVE_FRONTEND_TOOLS, 
   RETRIEVE_BACKEND_TOOLS,
   SET_FRONTEND_LOADER,
   SET_BACKEND_LOADER,
   FRONTEND_ERROR_ENCOUNTERED,
   BACKEND_ERROR_ENCOUNTERED,
   TOGGLE_FRONTEND_COLLPASE,
   TOGGLE_BACKEND_COLLPASE
}