import { WEBSOCKET_CONNECT, WEBSOCKET_SEND } from '../Types/websocketTypes';
import { WSMESSAGE_USER_LOGIN, WSMESSAGE_USER_LIST_UPDATE, WSMESSAGE_MESSAGE_RECEIVED, WSMESSAGE_USER_LOGOUT, WSMESSAGE_MESSAGE_SENT } from '../Types/SocketCommunicationTypes';

let connectToServer = () => async dispatch => {
   dispatch({type: WEBSOCKET_CONNECT});
};

let loginToServer = (data) => async dispatch => {
    let loginObject = {
        type: WSMESSAGE_USER_LOGIN,
        payload: {
            content: data,
        }
    }
    dispatch({type: WEBSOCKET_SEND, payload: loginObject});
}

let logoutOfServer = (data) => async dispatch => {
    let logoutObject = {
        type: WSMESSAGE_USER_LOGOUT,
        payload: {
            content: data
        }
    }
    dispatch({type: WEBSOCKET_SEND, payload: logoutObject});
}

let getUserList = () => async dispatch => {
    let getUserListReq = {
        type: WSMESSAGE_USER_LIST_UPDATE,
        payload: {
            content: ''
        }
    }
    dispatch({type: WEBSOCKET_SEND, payload: getUserListReq});
}

let sendMessage = (data) => async dispatch => {
    let messageObject = {
        type: WSMESSAGE_MESSAGE_RECEIVED,
        payload: {
            content: data
        }
    }
    await dispatch({type: WSMESSAGE_MESSAGE_SENT});
    dispatch({type: WEBSOCKET_SEND, payload: messageObject});
}

export {
    connectToServer,
    loginToServer,
    logoutOfServer,
    getUserList,
    sendMessage,
}