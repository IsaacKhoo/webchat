import { TOGGLE_DRAWER } from '../Types/Types';

// drawer methods
let toggleDrawer = () => async dispatch => {
   dispatch({type: TOGGLE_DRAWER});
}

export {
   toggleDrawer,
}