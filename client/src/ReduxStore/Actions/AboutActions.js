import {
  RETRIEVE_FRONTEND_TOOLS,
  RETRIEVE_BACKEND_TOOLS,
  SET_FRONTEND_LOADER,
  SET_BACKEND_LOADER,
  FRONTEND_ERROR_ENCOUNTERED,
  BACKEND_ERROR_ENCOUNTERED,
  TOGGLE_FRONTEND_COLLPASE,
  TOGGLE_BACKEND_COLLPASE
} from "../Types/Types";
import axios from "axios";

// about actions
let getFrontendTools = () => async dispatch => {
  try {
    dispatch({ type: SET_FRONTEND_LOADER });
    let res = await axios.get("/api/frontend");
    await dispatch({ type: RETRIEVE_FRONTEND_TOOLS, payload: res.data });
    setTimeout(() => {
      dispatch({ type: TOGGLE_FRONTEND_COLLPASE });
    }, 300);
  } catch (err) {
    console.log(err);
    dispatch({ type: FRONTEND_ERROR_ENCOUNTERED });
  }
};

let getBackendTools = () => async dispatch => {
  try {
    dispatch({ type: SET_BACKEND_LOADER });
    let res = await axios.get("/api/backend");
    await dispatch({ type: RETRIEVE_BACKEND_TOOLS, payload: res.data });
    setTimeout(() => {
      dispatch({ type: TOGGLE_BACKEND_COLLPASE });
    }, 300);
  } catch (err) {
    console.log(err);
    dispatch({ type: BACKEND_ERROR_ENCOUNTERED });
  }
};

export { getFrontendTools, getBackendTools };
