import Alert from 'react-s-alert';

import { WEBSOCKET_MESSAGE } from "../Types/websocketTypes";
import {
  WSMESSAGE_MESSAGE_RECEIVED,
  WSMESSAGE_USER_ADD,
  WSMESSAGE_USER_REMOVE,
  WSMESSAGE_USER_LIST_UPDATE,
  WSMESSAGE_USER_LOGIN,
  WSMESSAGE_USER_LOGOUT,
  WSCONNECT,
  WSMESSAGE_MESSAGE_SENT
} from "../Types/SocketCommunicationTypes";

let initialState = {
  messagesSent: 0,
  onlineTime: Date(),
  isConnected: false,
  currentUsername: "",
  messageList: [],
  userList: [],
  log: []
};

let getTimeNow = () => {
  let d = new Date();
  let h = (d.getHours()<10? '0'+ d.getHours() : (d.getHours() > 12 ? d.getHours() - 12 : d.getHours()));
  let m = (d.getMinutes()<10?'0':'') + d.getMinutes();
  let n = (d.getHours() > 12 ? "PM" : "AM");
  return (h + ':' + m + ' ' + n);
}

export default (state = initialState, action) => {
  switch (action.type) {
    case WSCONNECT: {
      if (state.isConnected === false) {
        return { ...state, isConnected: !state.isConnected };
      }
      return state;
    }
    case WSMESSAGE_MESSAGE_SENT: {
      return {...state, messagesSent: state.messagesSent + 1}
    }
    case WEBSOCKET_MESSAGE: {
      let messageObject = JSON.parse(action.payload);
      console.log(`In reducer: ${JSON.stringify(messageObject)}`);
      let newState = { ...state, log: [...state.log, messageObject] };

      let messageType = messageObject.type;
      let messagePayload = messageObject.payload;

      let payloadString = JSON.stringify(messagePayload);

      switch (messageType) {
        case WSMESSAGE_USER_LOGIN: {
          newState = { ...newState, currentUsername: messagePayload.content, onlineTime: getTimeNow()};
          return newState;
        }
        case WSMESSAGE_USER_LOGOUT: {
          newState = { ...newState, messageList: [], userList:[], isConnected: false, currentUsername: "" };
          return newState;
        }
        case WSMESSAGE_MESSAGE_RECEIVED: {
          newState = {
            ...newState,
            messageList: [...newState.messageList, messagePayload.content]
          };
          if(messagePayload.content.user !== state.currentUsername) {
            Alert.info(messagePayload.content.msg, {
              position: 'bottom-right',
              customFields: {
                username: messagePayload.content.user,
              }
            })
          }         
          return newState;
        }      
        case WSMESSAGE_USER_LIST_UPDATE: {
          if (Array.isArray(messagePayload.content)) {
            newState = { ...newState, userList: messagePayload.content };
          }
          return newState;
        }
        case WSMESSAGE_USER_ADD: {
          newState = {
            ...newState,
            userList: [...newState.userList, messagePayload.content]
          };
          return newState;
        }
        case WSMESSAGE_USER_REMOVE: {
          let userObjToRemove = messagePayload.content;
          let userListCopy = newState.userList;
          let index = -1;
          for(let i = 0; i < userListCopy.length; i++) {
            if(userListCopy[i].name === userObjToRemove.name) {
              index = i;
              break;
            }
          }
          if (index > -1) {
            userListCopy.splice(index, 1);
            newState = { ...newState, userList: userListCopy };
            return newState;
          } else {
            return state;
          }
        }
        default:
          return newState;
      }
    }
    default:
      return state;
  }
};
