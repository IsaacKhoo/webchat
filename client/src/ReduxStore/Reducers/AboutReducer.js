import {
  RETRIEVE_FRONTEND_TOOLS,
  RETRIEVE_BACKEND_TOOLS,
  SET_BACKEND_LOADER,
  SET_FRONTEND_LOADER,
  FRONTEND_ERROR_ENCOUNTERED,
  BACKEND_ERROR_ENCOUNTERED,
  TOGGLE_FRONTEND_COLLPASE,
  TOGGLE_BACKEND_COLLPASE
} from "../Types/Types";

let initialState = {
  frontEndContent: [],
  backEndContent: [],
  frontEndLoading: false,
  backEndLoading: false,
  errorMsg: "Unable to retrieve content from server now",
  frontEndShowError: false,
  backEndShowError: false,
  displayFrontendCollapse: false,
  displayBackendCollapse: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_FRONTEND_LOADER: {
      return { ...state, frontEndLoading: true };
    }
    case SET_BACKEND_LOADER: {
      return { ...state, backEndLoading: true };
    }
    case RETRIEVE_FRONTEND_TOOLS: {
      return {
        ...state,
        frontEndContent: action.payload,
        frontEndLoading: false
      };
    }
    case RETRIEVE_BACKEND_TOOLS: {
      return {
        ...state,
        backEndContent: action.payload,
        backEndLoading: false
      };
    }
    case FRONTEND_ERROR_ENCOUNTERED: {
      return { ...state, frontEndLoading: false, frontEndShowError: true };
    }
    case BACKEND_ERROR_ENCOUNTERED: {
      return { ...state, backEndLoading: false, backEndShowError: true };
    }
    case TOGGLE_FRONTEND_COLLPASE: {
      return { ...state, displayFrontendCollapse: true };
    }
    case TOGGLE_BACKEND_COLLPASE: {
      return { ...state, displayBackendCollapse: true };
    }
    default:
      return state;
  }
};
