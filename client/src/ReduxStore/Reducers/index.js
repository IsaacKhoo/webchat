import { combineReducers } from 'redux';

import drawerReducer from './DrawerReducer';
import WSReducer from './WebSocketReducers';
import aboutReducer from './AboutReducer';

export default combineReducers({
   drawer: drawerReducer, 
   ws: WSReducer,
   abt: aboutReducer,
})