import io from "socket.io-client";
import {
  WEBSOCKET_CONNECT,
  WEBSOCKET_SEND,
  WEBSOCKET_MESSAGE,
  WEBSOCKET_DISCONNECT
} from "../Types/websocketTypes";
import {
  WSCONNECT,
  WSMESSAGE_MESSAGE_SENT,
  WSMESSAGE_MESSAGE_RECEIVED
} from "../Types/SocketCommunicationTypes";

let ws = null;

let injectURL = () => {
  if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
    // development environment
    return "http://115.66.126.234:5000";
  } else {
    // prod
    return "";
  }
};

let handleConnect = () => {
  return {
    type: WSCONNECT,
    payload: ""
  };
};

let handleMessage = message => {
  return {
    type: WEBSOCKET_MESSAGE,
    payload: message
  };
};

let createMiddleware = () => {
  let initialize = ({ dispatch }) => {
    if (!ws) {
      ws = io(injectURL());
    }

    ws.on("connect", () => dispatch(handleConnect()));

    ws.on("event", event => dispatch(handleMessage(event)));

    ws.on("disconnect", () => {
      close();
      console.log(`websocket closed.`);
    });
  };

  let close = () => {
    if (ws) {
      ws.close();
      ws = null;
    }
  };

  return store => next => action => {
    switch (action.type) {
      case WEBSOCKET_CONNECT: {
        initialize(store);
        next(action);
        break;
      }
      case WEBSOCKET_DISCONNECT: {
        close();
        next(action);
        break;
      }
      case WEBSOCKET_SEND: {
        if (ws) {
          ws.emit("event", JSON.stringify(action.payload));
        } else {
          console.log(`socket not connected`);
        }
        next(action);
        break;
      }
      default:
        next(action);
    }
  };
};

export default createMiddleware();
