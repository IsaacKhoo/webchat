import React from "react";
import ReactDOM from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
import "./Assets/css/styles.css";
import { Provider } from "react-redux";

import store from "./ReduxStore/Store/Store";
import App from './TestingComponent/App/App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
