import { drawerWidth } from './common';

let appStyles = theme => ({
   root: {
      flexGrow: 1,
      height: '100vh',
      zIndex: 1100,
      overflow: 'auto',
      position: 'relative',
      display: 'flex',
      width: '100vw',
   },
   content: {
      flexGrow: 1,
      padding: "5px",
      position: 'relative',
      [theme.breakpoints.up('md')]: {
         width: `(100vw - ${drawerWidth}px)`,
      },
      width: '100vw',
   },
   toolbar: theme.mixins.toolbar,
});

export default appStyles;