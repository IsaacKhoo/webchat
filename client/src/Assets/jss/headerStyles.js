import { drawerWidth } from './common';

let headerStyles = theme => ({
   appBar: {
      position: 'fixed',
      marginLeft: drawerWidth,
      [theme.breakpoints.up('md')] : {
         width: `calc(100% - ${drawerWidth}px)`
      }
   },
});

export default headerStyles;