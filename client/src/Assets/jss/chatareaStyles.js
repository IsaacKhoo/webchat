
let chatareaStyles = theme => ({
    otherMsg: {
        justifyContent: 'flex-start',
    },
    userMsg: {
        justifyContent: 'flex-end',
    },
    listContainer: {
        width: '100%',
    },
    chatPaper: {
        padding: '5px',
    },
    chatBox: {
        position: 'absolute',
        display: 'flex',
        flexGrow: 1,
        top: 70,
        width: '97.5%',
        height: `calc(100% - 140px)`,
        backgroundColor: '#a3c0ed',
        padding: '10px',
        overflowY: 'scroll',
    },
    inputPaper: {
      padding: '10px',
      display: 'flex',
      position: 'absolute',
      left: 0,
      right: 0,
      bottom: 5,
   },
   inputField: {
      flex: '9'
   },
   inputSendButton: {
      flex: '1',
      maxWidth: '30px',
      maxHeight: '30px',
   }
});

export default chatareaStyles;