import { drawerWidth } from './common';

let sidebarStyles = theme => ({
    outerContainer: {
        position: 'absolute',
        top:0,
        left:0,
    },
    sidebarContainer: {
        position: 'fixed',
        top: 0,
        left: 0,
    },
   drawerPaper: {
      width: drawerWidth,
      [theme.breakpoints.up('md')] : {
         position: 'relative',
      },
      height: '100vh',
      zIndex: 1050,
   },
   toolbar: theme.mixins.toolbar,
});

export default sidebarStyles;