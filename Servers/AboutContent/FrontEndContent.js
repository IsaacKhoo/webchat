module.exports = [
   {
      title: "React, React-Router-Dom, React-Dom",
      description: "Used for creating the entire front end layout and views"
   },
   {
      title: "Redux, React-Redux, Redux-Thunk, Prop-Types",
      description: "State management for react to handle user interaction"
   },
   {
      title: "Socket.io-client, Axios",
      description: `Websocket connection to server for live updates to chat.
      Axios to access server REST API to pull content`
   },
   {
      title: "ClassNames, Ifvisible, React-Scroll, React-s-alert, Lodash",
      description: "Supporting packages to aid in building the application"
   }
]
