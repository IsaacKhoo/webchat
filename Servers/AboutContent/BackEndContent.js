module.exports = [
   {
      title: `Express, Body-parser`,
      description: `REST API server and JSON parser communication tool`
   },
   {
      title: `Socket.io`,
      description: `Websocket for pushing live updates to connected clients`
   },
   {
      title: `Git`,
      description: `Application development version management`
   },
   {
      title: `Heroku`,
      description: `Application cloud hosting services`
   },
   {
      title: `Nodemon, Concurrently, Cookies`,
      description: `Supporting tools development`
   },
]