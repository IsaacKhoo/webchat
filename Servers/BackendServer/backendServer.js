const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const socketIO = require('socket.io');
const ClientHandler = require('./ClientHandler');
const CloseHandler = require('./Closehandler');

const PORT = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

// start up server and websocket
app.use(bodyParser.json());
server.listen(PORT, () => { console.log(`server started on ${PORT}`) });
io.on('connection', (clientSocket) => {
   console.log(`client ${clientSocket.client.id} connected`);

   clientSocket.on('event', (data) => {
      console.log(`received: ${data}`);
      ClientHandler(clientSocket, JSON.parse(data));
   });

   clientSocket.on('disconnect', () => {
      console.log(`client ${clientSocket.client} disconnected`);
      CloseHandler(clientSocket);
   })
})

// get routes to work
require('./expressRoutes')(app);

// setup for prod heroku
if(process.env.NODE_ENV === 'production') {
   // express will serve up production assets
   // such as main.js and main.css files
   app.use(express.static('client/build'));

   // express will serve up the index.html file 
   // if it doesnt recognise the route.
   const path = require('path');
   app.get('*', (req, res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
   });
}