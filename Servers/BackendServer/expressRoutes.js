let frontendContent = require('../AboutContent/FrontEndContent');
let backendContent = require('../AboutContent/BackEndContent');

module.exports = (app) => {
   app.get('/api/frontend', (req, res) => {
      setTimeout(() => {
         res.send(frontendContent);
      }, 2000);
   })

   app.get('/api/backend', (req, res) => {
      res.send(backendContent);
   })
}