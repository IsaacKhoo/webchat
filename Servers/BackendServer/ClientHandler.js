let handlerConstants = require("./handleTypes");
let records = require('./records');
let closeHandler = require('./Closehandler');

let clientList = records.ClientList;
let usernames = records.Usernames;
let messageList = records.MessageList;

module.exports = ClientHandler = (clientSocket, messageObject) => {
  let dater = new Date();
  let date = dater.getDate();
  let hours = dater.getHours();
  let minutes = (dater.getMinutes() < 10 ? "0" : '') + dater.getMinutes();
  let time = `${hours}:${minutes}`;

  let replyObject = {
    type: messageObject.type,
    payload: {
       content: '',
       dateStamp: date,
       timeStamp: time,
    }
  };

  switch (messageObject.type) {
    case handlerConstants.WSMESSAGE_USER_LOGIN: {
       console.log(`entered login case`);
       let username = messageObject.payload.content; 
       let change = 0;
       while(usernames.indexOf(username) > -1) {
         change++; 
         username = username + "_" + change.toString();
       }
       clientList.push({username: username, socket: clientSocket });
       usernameObject = {
         name: username,
         color: getRandomColor(),
       }
       usernames.push(usernameObject);

       // send back username to user
       replyObject.payload.content = username;
       send(clientSocket, replyObject);

       // give user the user list
       replyObject.type = handlerConstants.WSMESSAGE_USER_LIST_UPDATE;
       replyObject.payload.content = usernames;
       send(clientSocket, replyObject);

       // add user to other users user list
       replyObject.type = handlerConstants.WSMESSAGE_USER_ADD;
       replyObject.payload.content = usernameObject;
       clientList.forEach(socObject => {
          if(socObject.socket !== clientSocket) {
             send(socObject.socket, replyObject);
          }
       });
       break;
    }
    case handlerConstants.WSMESSAGE_USER_LOGOUT: {
      console.log(`entered logout case`);
      send(clientSocket, replyObject);
      closeHandler(clientSocket);
      break;
    }
    case handlerConstants.WSMESSAGE_USER_LIST_UPDATE: {
      break;
    }
    case handlerConstants.WSMESSAGE_MESSAGE_RECEIVED: {
      console.log(`entered msg received use case`);
      console.log(`${JSON.stringify(messageObject.payload)}`);
      let messageReceived = messageObject.payload.content;
      messageList.push(messageReceived);

      // broadcast message to all
      replyObject.payload.content = messageReceived;

      clientList.forEach(socObject => {
        send(socObject.socket, replyObject);
     });
      break;
    }
    default: {
      console.log(`received unrecognised command: ${JSON.stringify(messageObject)}`);
      clientSocket.disconnect(true);
      break;
    }
  }
};

let send = (socket, obj) => {
   if(socket) {
      socket.emit('event',JSON.stringify(obj));
   }
}

let getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}