let handlerConstants = require("./handleTypes");
let records = require("./records");

let clientList = records.ClientList;
let usernames = records.Usernames;

module.exports = CloseHandler = clientSocket => {
  let clientIndex = -1;
  let usernameIndex = -1;
  let username = "";

  let dater = new Date();
  let date = dater.getDate();
  let hours = dater.getHours();
  let minutes = (dater.getMinutes() < 10 ? "0" : "") + dater.getMinutes();
  let time = `${hours}:${minutes}`;

  let userDisconnectedObject = {
    type: handlerConstants.WSMESSAGE_USER_REMOVE,
    payload: {
      content: "",
      dateStamp: date,
      timeStamp: time
    }
  };

  for (let i = 0; i < clientList.length; i++) {
    if (clientList[i].socket === clientSocket) {
      clientIndex = i;
      break;
    }
  }

  if (clientIndex !== -1) {
    for(let i = 0; i < usernames.length; i++) {
      if(usernames[i].name === clientList[clientIndex].username) {
        usernameIndex = i;
        break;
      }
    }
  }

  if (usernameIndex !== -1) {
    usernameObj = usernames[usernameIndex];

    // remove from username list
    usernames.splice(usernameIndex, 1);

    // remove from client list
    clientList.splice(clientIndex, 1);
    console.log(`removed client from list`);

    //broadcast to all to remove disconnecting user
    userDisconnectedObject.payload.content = usernameObj;
    clientList.length
      ? clientList.forEach(socObject => {
          if (socObject.socket !== clientSocket) {
            send(socObject.socket, userDisconnectedObject);
          }
        })
      : null;
      console.log(`broadcasting client exit`);
  }
};

let send = (socket, obj) => {
  if(socket) {
     socket.emit('event',JSON.stringify(obj));
  }
}